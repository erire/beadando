#include "scene.h"

#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>

double rotation = 0;
float ambient_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float diffuse_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float specular_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float position[] = { 0.0f, 1.0f, 10.0f, 0.0f };


void init_scene(Scene* scene)
{
    load_model(&(scene->cube), "gun.obj");
	
	/*scene->texture_id = load_texture("box3.png");
	
	
	unsigned int i;
	
	char texture_filenames[][32] = {
        "box2.png",
        "box3.png",
    };
	
	for (i = 0; i < 2; i++) {
        texture_names[i] = load_texture(texture_filenames[i], images[i]);
    }*/
    

    scene->material.ambient.red = 0.24725;
    scene->material.ambient.green = 0.1995;
    scene->material.ambient.blue = 0.0745;

    scene->material.diffuse.red = 0.75164;
    scene->material.diffuse.green = 0.60648;
    scene->material.diffuse.blue = 0.22648;

    scene->material.specular.red = 0.628281;
    scene->material.specular.green = 0.555802;
    scene->material.specular.blue = 0.366065;

    scene->material.shininess = 0.4;
	
}

void initialize_texture()
{
    texture_names[0] = load_texture("box2.png", images[0]);
    texture_names[1] = load_texture("box3.png", images[1]);
	glEnable(GL_TEXTURE_2D);
}

void set_lighting()
{
    

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
}

void set_material(const Material* material)
{
    float ambient_material_color[] = {
        material->ambient.red,
        material->ambient.green,
        material->ambient.blue
    };

    float diffuse_material_color[] = {
        material->diffuse.red,
        material->diffuse.green,
        material->diffuse.blue
    };

    float specular_material_color[] = {
        material->specular.red,
        material->specular.green,
        material->specular.blue
    };

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material_color);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &(material->shininess));
}

void draw_scene(const Scene* scene)
{
    set_material(&(scene->material));
	
    set_lighting();
	glRotatef(90,1,0,0);
	glRotatef(rotation,0,1,0);
    draw_origin();
	glBindTexture(GL_TEXTURE_2D, texture_names[1]);
    draw_model(&(scene->cube));
}

void draw_origin()
{
    
	glBegin(GL_LINES);

    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(1, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 1, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 1);

    glEnd();
}

/*void update_scene(double  delta) 
{
	
	rotation += delta *10;
}*/

void update_scene(char asd) 
{
	
	
	double delta = 0.08;
	if (asd=='+'){
		rotation += delta *10;
	}
	else if (asd=='-'){
		rotation -= delta *10;
	}
	
}


void update_light(char key) 
{
	
	 if(key=='q') {
		ambient_light[0] += 1.0f;
		ambient_light[1] += 1.0f;
		ambient_light[2] += 1.0f;
		ambient_light[3] += 1.0f;
		
		
        
	}
	else if(key=='e') {
		ambient_light[0] -= 1.0f;
		ambient_light[1] -= 1.0f;
		ambient_light[2] -= 1.0f;
		ambient_light[3] -= 1.0f;
  
	}
	
        
       
}
